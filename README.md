# GitLab Stale Issue Bot

The intention is for this bot to browse all issues on the GitLab CE project and add the "Awaiting Feedback" label to issues that have become stale.

The criteria for adding the label is as-follows:

- No new responses for 3 months (not including system notes?)
- No milestone
- No assignee
- Possibly no label?

If those criterion are met, the bot applies the label and comments "This issue has become stale, is this still a problem?". If the issue recieves no response for 14 days, the bot will comment "Closing due to lack of response." and the issue will be closed.

This makes outdated issues less of a maintenance burden, and team members can focus on issues that actually effect current releases.

## Resources

Building the gem:
- [gitlab gem documentation](http://www.rubydoc.info/gems/gitlab/3.4.0)
- [gitlab gem github repo](https://github.com/NARKOZ/gitlab)
- [Date class in Ruby](http://ruby-doc.org/stdlib-2.3.1/libdoc/date/rdoc/Date.html)
- [GitLab API Docs](http://doc.gitlab.com/ce/api/README.html)

Blog posts on managing an open source project:
- [Tips for keeping your Open Source Software issues tracker tidy](http://blog.plataformatec.com.br/2014/05/tips-for-keeping-your-open-source-software-issues-tracker-tidy/)
- [The Art of Closing](https://blog.jessfraz.com/post/the-art-of-closing/)
- [Managing the Deluge of Atom Issues](http://blog.atom.io/2016/04/19/managing-the-deluge-of-atom-issues.html)
- [How to be an open source gardener](http://words.steveklabnik.com/how-to-be-an-open-source-gardener)
- [Handling Large OSS Projects Defensively](http://artsy.github.io/blog/2016/07/03/handling-big-projects/)
- [My condolences, you’re now the maintainer of a popular open source project](https://runcommand.io/2016/06/26/my-condolences-youre-now-the-maintainer-of-a-popular-open-source-project/)

## Setup

Follow these steps to get this bot working:

1. Pull down the repository
1. Generate an Access Token [in your Profile Settings on GitLab.com](https://gitlab.com/profile/personal_access_tokens)
1. Run `export GITLAB_API_ACCESS_TOKEN=AccessTokenHere`, e.g. `export GITLAB_API_ACCESS_TOKEN=iZ3x2mP5fVBtwpyabcq5`
1. Run the script with `ruby bot.rb`

## Known Issues, Caveats

- The bot will get stuck for 60 seconds before timing out due to certain issues which are for whatever reason corrupt.
- The repository it's checking can't be customized right now.
- The GitLab instance cannot be customized right now, locked to GitLab.com.
- The script must be run manually, ideally it'd be hosted on Heroku or some other service.
- The bot doesn't actually post or modify anything at the moment, just lists the issues that are open without activity in the last 90 days.
