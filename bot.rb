require "gitlab"

# 789333 - ID of my fork of CE
# 13083  - ID of CE

GITLAB_CE = 13083
@counter = 0

def start
  bot = Gitlab.client(endpoint: 'https://gitlab.com/api/v3', private_token: ENV['GITLAB_API_ACCESS_TOKEN'])

  puts bot.project(13083).name

  issue_count = bot.project(13083).open_issues_count
  pages = issue_count / 20

  puts "There are currently #{issue_count} open issues."

  puts "There are currently #{pages} pages of issues."

  do_stuff(bot, issue_count, pages)
end

def do_stuff(bot, issue_count, pages)
  # List all issues in the project.
  (0..pages - 1).reverse_each do |num|
    begin
      issues = bot.issues(GITLAB_CE, page: num, per_page: 20)

      (0..19).each do |num2|
        # puts issues[num].labels.empty?
        # puts issues[num].updated_at

        date = Date.parse(issues[num2].updated_at)
        date_diff = Date.today - date
        max_date_diff = 90

        # Prints the difference between the two dates.
        puts date_diff

        if date_diff > max_date_diff
          puts "3+ MONTHS SINCE LAST UPDATE"
          iterate_counter()
        end
      end

    rescue
      puts "Error"
    end
  end

  puts @counter
end

def iterate_counter
  @counter += 1
end

start
